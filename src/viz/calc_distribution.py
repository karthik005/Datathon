#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-11-28 15:34:21

import pandas as pd
import numpy as np


def load_data(data_file_name, headers_file):
	loaded_df = pd.read_csv(data_file_name, header=None)
	headers = pd.read_csv(headers_file, header=None).loc[:,0]
	loaded_df.columns = headers
	return loaded_df

def get_location_dist(loaded_df, param_dict,  grid_size = 1):
	filtered_df = loaded_df.copy()

	for param in param_dict:
		if isinstance(param_dict[param], list) and len(param_dict[param]) == 2:
			low, high = param_dict[param]
			filtered_df = filtered_df[(filtered_df[param] >= low) & (filtered_df[param] <= high)]

		else:
			filtered_df = filtered_df[filtered_df[param] == param_dict[param]]

	bin_dim = [180/grid_size, 360/grid_size]
	histo, _, _ = np.histogram2d(filtered_df.Longitude, filtered_df.Latitude, bins=bin_dim)
	distribution = histo/len(filtered_df)

	distribution = np.nan_to_num(distribution)
	return distribution, len(filtered_df)


def main():
	file_name = "../../data/dataset_1mb.csv"
	headers_name = "../../sample_data/column_names.csv"
	loaded_df = load_data(file_name, headers_name)
	np.set_printoptions(threshold=np.nan)
	distribution, sizer = get_location_dist(loaded_df, {"Age" : [20,35]})
	print distribution
	print sizer

if __name__ == '__main__':
	main()