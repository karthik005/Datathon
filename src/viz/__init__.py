#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-11-28 15:34:02
from calc_distribution import *
import cherrypy as cp
from cherrypy.lib.static import serve_file
import simplejson as sj
import json
import os.path
import numpy as np


current_dir = os.path.dirname(os.path.abspath(__file__))

'''
cp backend module for handling data transmission
'''
file_name = "../../data/dataset_1mb.csv"
headers_name = "../../sample_data/column_names.csv"
loaded_df = load_data(file_name, headers_name)

class Root:

	@cp.expose
	def index(self):
		print current_dir
		return serve_file(current_dir+"/static/index.html")
		# return "hello world"

	@cp.expose
	@cp.tools.json_out()
	@cp.tools.json_in()
	def exchange_data(self):
		param_dict = cp.request.json
		print param_dict

		distribution, tot_num = get_location_dist(loaded_df, param_dict)

		row_indices, col_indices = np.nonzero(distribution)
		values = [distribution[r][c] for r,c in zip(row_indices, col_indices)]
		distribution_comp = zip(90-row_indices, col_indices-180, values)

		json_string = sj.dumps({"distribution":distribution_comp, "total_num" : tot_num})

		return json_string


cp.quickstart(Root(), '/', config = "dev.conf")
