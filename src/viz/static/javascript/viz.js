/* 
* @Author: Karthik
* @Date:   2015-11-28 17:56:05
* @Last Modified by:   Karthik
* @Last Modified time: 2015-11-29 09:37:10
*/


var width = 900,
height = 800,
sens = 0.25,
focused;
var Points = [];
  //Setting projection

  var projection = d3.geo.orthographic()
  .scale(245)
  .rotate([0, 0])
  .translate([width / 2, height / 2])
  .clipAngle(90);

  var path = d3.geo.path()
  .projection(projection);

  //SVG container

  var svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height);

  //Adding water

  svg.append("path")
  .datum({type: "Sphere"})
  .attr("class", "water")
  .attr("d", path)
  .call(d3.behavior.drag()
    .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
    .on("drag", function() {
      var rotate = projection.rotate();
      projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
      svg.selectAll("path.land").attr("d", path);
      svg.selectAll(".focused").classed("focused", focused = false);
    }));

  // var countryTooltip = d3.select("body").append("div").attr("class", "countryTooltip");
  // countryList = d3.select("body").append("select").attr("name", "countries");


  queue()
  .defer(d3.json, "resources/world.json")
  .defer(d3.tsv, "resources/names.tsv")
  .await(ready);

  //Main function

  function ready(error, world, countryData) {

    var countryById = {},
    countries = topojson.feature(world, world.objects.countries).features;

    //Adding countries to select

    // countryData.forEach(function(d) {
    //   countryById[d.id] = d.name;
    //   option = countryList.append("option");
    //   option.text(d.name);
    //   option.property("value", d.id);
    // });

    //Drawing countries on the globe

    var world = svg.selectAll("path.land")
    .data(countries)
    .enter().append("path")
    .attr("class", "land")
    .attr("d", path)


    //Drag event

    .call(d3.behavior.drag()
      .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
      .on("drag", function() {
        var rotate = projection.rotate();
        projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
        svg.selectAll("path.land").attr("d", path);
        svg.selectAll(".focused").classed("focused", focused = false);

        // update globe
        svg.selectAll("path.cities").attr("d", path);

        $.each(Points, function(index, value){
          svg.insert("path", ".foreground")
          .datum({type: "Point", coordinates:[value[0],value[1]]})
          // .attr("class", "point")
          .attr("class", "cities")
          .attr("d", path)
          .attr("fill", "red")
          .attr("fill-opacity", 0.5);
        });
      }));

    //Mouse events

    // .on("mouseover", function(d) {
    //   countryTooltip.text(countryById[d.id])
    //   .style("left", (d3.event.pageX + 7) + "px")
    //   .style("top", (d3.event.pageY - 15) + "px")
    //   .style("display", "block")
    //   .style("opacity", 1);
    // })
    // .on("mouseout", function(d) {
    //   countryTooltip.style("opacity", 0)
    //   .style("display", "none");
    // })
    // .on("mousemove", function(d) {
    //   countryTooltip.style("left", (d3.event.pageX + 7) + "px")
    //   .style("top", (d3.event.pageY - 15) + "px");
    // });

    //Country focus on option select

    d3.select("select").on("change", function() {
      var rotate = projection.rotate(),
      focusedCountry = country(countries, this),
      p = d3.geo.centroid(focusedCountry);

      svg.selectAll(".focused").classed("focused", focused = false);

    //Globe rotating

    (function transition() {
      d3.transition()
      .duration(2500)
      .tween("rotate", function() {
        var r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
        return function(t) {
          projection.rotate(r(t));
          svg.selectAll("path").attr("d", path)
          .classed("focused", function(d, i) { return d.id == focusedCountry.id ? focused = d : false; });
        };
      })
    })();
  });

    function country(cnt, sel) { 
      for(var i = 0, l = cnt.length; i < l; i++) {
        if(cnt[i].id == sel.value) {return cnt[i];}
      }
    };

    $('input').change(function(){

      var data_dict = {};
        // get all checked input boxes
        $('.checkbox-custom:checked').each(function(){
          var value = $(this).attr('id');
          var param = $(this).attr('param');

          data_dict[param] = value;
        });

        $.ajax({
          url: "exchange_data",
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
          },
          type: "POST",
          data: JSON.stringify(data_dict),
          contentType:'application/json; charset=utf-8',
          dataType: 'json',

          success: function(response){
            response = JSON.parse(response);
            // console.log(response);

            Points = response['distribution'];
            var total = response['total_num'];

             // update globe
             svg.selectAll("path.cities").attr("d", path);

             $.each(Points, function(ndx, value){
              svg.insert("path", ".foreground")
              .datum({type: "Point", coordinates:[value[0],value[1]]})
              .attr("class", "cities")
              .attr("d", path)
              .attr("fill", "red")
              .attr("fill-opacity", 0.5);
            });
           }

         });
      });

};
