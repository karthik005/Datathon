#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-11-29 00:20:01
# @Last Modified by:   chandan
# @Last Modified time: 2015-11-29 02:35:46

import numpy as np
import pandas as pd

def get_data(fname):
	df = pd.read_csv(fname, header=None)
	return df

def get_columns(attr_fname):
	df = pd.read_csv(attr_fname, header=None)
	return df.loc[:, 0]

def main():
	pass

if __name__ == '__main__':
	main()