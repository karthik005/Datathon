#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-11-29 01:49:16
# @Last Modified by:   chandan
# @Last Modified time: 2015-11-29 09:40:29

# DATA_FILE = '/media/OS/data/hour0_sliced.csv'
DATA_FILE = '../../data_sample_1mb.csv'
ATTRS_FILE = '../../data_sample_attributes.csv'
CAT_FEATS = ['TrafficType','PublisherId', 'AppSiteId', 'AppSiteCategory', 'Position', 
			'Gender', 'OS', 'Model', 'Manufacturer','Carrier','DeviceType',
			'DeviceId','DeviceIP','Country','GeoType','CreativeCategory']

# SELECT_FEATS = ['PublisherId', 'AppSiteId', 'AppSiteCategory', 'Position', 'Latitude', 'Longitude', 'ExchangeBid']
SELECT_FEATS = ['ExchangeBid']

import pandas as pd
import numpy as np

import data_handlers as dh
import file_handlers as fh

from sklearn import preprocessing
from sklearn import cross_validation
from sklearn import svm

def read_fit_data(data_file, feat_file, cat_feats):
	df = fh.get_data(DATA_FILE)
	cols = fh.get_columns(ATTRS_FILE)
	df = dh.add_cols_to_df(df, cols)
	encoder = preprocessing.LabelEncoder()

	for cat_feat in cat_feats:
		df[cat_feat] = encoder.fit_transform(df[cat_feat])

	return df

def pred_bid_success(df, feats):
	# select features
	df_feats = df.loc[:, [col for col in set(df.columns).intersection(feats)]]
	labels = np.array([1 if elem=='w' or elem == 'c' else 0 for elem in df['Outcome']])

	# X_train, X_test, y_train, y_test = cross_validation.train_test_split(df_feats, labels, test_size=0.4, random_state=0)

	print len(df), len(labels)

	delim = int(0.7*len(labels))

	# training data
	X_train = np.array(df_feats)[:delim, :]
	y_train = labels[:delim]

	# test data
	X_test = np.array(df_feats)[delim:,:]
	y_test = labels[delim:]

	# train an SVM
	clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
	score = clf.score(X_test, y_test)

	return score


if __name__ == '__main__':
	df = read_fit_data(DATA_FILE, ATTRS_FILE, CAT_FEATS)
	score = pred_bid_success(df, CAT_FEATS)

	print score
