#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-11-29 00:14:50
# @Last Modified by:   chandan
# @Last Modified time: 2015-11-29 02:17:46

DATA_FILE = '/media/OS/data/hour0_sliced.csv'
ATTRS_FILE = '../../data_sample_attributes.csv'

import numpy as np
import pandas as pd

import file_handlers as fh
import data_handlers as dh

def main():
	df = fh.get_data(DATA_FILE)
	cols = fh.get_columns(ATTRS_FILE)
	df = dh.add_cols_to_df(df, cols)
	return 

if __name__ == '__main__':
	main()