#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-11-28 15:47:03
# @Last Modified by:   chandan
# @Last Modified time: 2015-11-28 16:49:44

import pandas as pd
import numpy as np
from pprint import pprint

def main():
	df = pd.read_csv('../../sample_data/data_sample_1mb.csv', header=None)
	headers = pd.read_csv('../../sample_data/data_sample_attributes.csv', header=None).loc[:, 0]
	df.columns = headers

	gridsize = 1
	binz = [360/gridsize, 180/gridsize]
	print binz

	histo, _, _ = np.histogram2d(df.Longitude, df.Latitude, bins=binz)
	np.set_printoptions(threshold=np.nan)
	print histo
	print len(df), np.sum(histo)

if __name__ == '__main__':
	main()